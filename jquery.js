var contador = 0;
var ultimoInput = "";
var primeraFila = false;
var modificando = false;

$(document).ready(function () {
    var cuadros = $(".cuadros");
    var totalLineas = 0;

    function comprobarAccion(accion) {
        switch (accion) {

            // Solo activo añadir
            case "a":
                $(".nav button").prop("disabled", true);
                $(".acciones button").prop("disabled", true);
                $(".control button").prop("disabled", true);
                $(".anadir").prop("disabled", false);
                console.log("a");
                break;
            //Cuando queremos que se seteen las clases de primera y ultima fila
            case "b":
                $(".cuadros div:last-child").addClass("ultimaFila");
                $(".cuadros div:first-child").addClass("primeraFila");
                console.log("b");
                break;
            case "c":
                $(".nav button").prop("disabled", false);
                $(".acciones button").prop("disabled", false);
                $(".control button").prop("disabled", true);
                console.log("c");
                break;
            case  "d":
                $(".inicio").prop("disabled", false);
                $(".menosuno").prop("disabled", false);
                $(".masuno").prop("disabled", false);
                $(".fin").prop("disabled", false);
                console.log("d");

                break;
            // Caso cuando estamos en la última fila
            case "f":
                $(".inicio").prop("disabled", false);
                $(".menosuno").prop("disabled", false);
                $(".masuno").prop("disabled", true);
                $(".fin").prop("disabled", true);
                console.log("f");
                break;
            //Cuando seleccionamos modificar un elemento
            case "m":
                $(".nav button").prop("disabled", true);
                $(".acciones button").prop("disabled", true);
                $(".control button").prop("disabled", false);
                console.log("m");
                break;

            // Caso cuando estamos en la primera fila
            case "p":
                $(".inicio").prop("disabled", true);
                $(".menosuno").prop("disabled", true);
                $(".masuno").prop("disabled", false);
                $(".fin").prop("disabled", false);
                console.log("p");
                break;

        }


    }


    $(".anadir").click(function () {
        setH1("Añadiendo filas")
        $(".cuadros").children(".linea").removeClass("seleccionado ultimaFila");
        // Contador de filas
        contador++;
        // Fila que se añadira cada vez que pulsemos añadir
        let fila = '<div class="linea seleccionado ultimaFila" id="' + contador + '">' +
            ' <input type="text" class="contador" name="linea" value="' + contador + '" disabled>' +
            ' <input type="date" name="fecha" class="fecha">' +
            ' <input type="text" name="descripcion" class="descripcion" >' +
            ' <input type="text" name="precio" value="0">' +
            ' <input type="text" name="unidades" value="0">' +
            ' <input type="text" value="0" name="total" class="total" disabled>';
        // Añadimos la fila a div cuadros
        cuadros.append(fila);
        if (!primeraFila) {
            $(".cuadros>div:last-child").addClass("primeraFila");
            primeraFila = true;
        }
        $(".cuadros>div:last-child").children(".fecha").focus();
        comprobarAccion("m");
        modificando = true;
    });

    $(".cancelar").click(function () {
        if (!modificando) {
            $(".cuadros div:last-child").remove();
            setH1("Esperando");

        } else {
            if ($(".clone div").length) {
                $(".clone div").remove();
            }
            $(".seleccionado input").prop("disabled", true);
            comprobarAccion("c");
            setH1("Esperando");
        }
        modificando = false;

    });


    $(".aceptar").click(function () {
        if (!modificando) {
            $(".cuadros div:last-child").children("input").prop("disabled", true);
            comprobarAccion("c");
            setH1("Esperando");
        } else {
            if ($(".clone div").length) {
                $(".seleccionado").replaceWith($(".clone div").addClass("seleccionado"));
                $(".clone div").remove();


            }
            $(".cuadros div").children("input").prop("disabled", true);

            setH1("Esperando");
            comprobarAccion("c");
            comprobarAccion("b");

        }
        modificando = false;

    });

    $(".nav button").click(function () {
        let selec = $(".seleccionado");
        setH1("Navegando");
        $(".cuadros div").removeClass("seleccionado");
        switch ($(this).val()) {
            case "0":
                $(".primeraFila").addClass("seleccionado");
                break;
            case "1":
                selec.prev().addClass("seleccionado");
                break;
            case "2":
                selec.next().addClass("seleccionado");
                break;
            case "3":
                $(".ultimaFila").addClass("seleccionado");
                break;
        }
        comprobarAccion("d");
    });


    $(".borrar").click(function () {
        $(".seleccionado").remove();
        $(".cuadros div:last-child").addClass("seleccionado");

        setH1("Borrando filas");


    });

    $(".modificar").click(function () {
        $(".seleccionado").clone().appendTo(".clone").removeClass("seleccionado ultimaFila primeraFila").find("input").prop("disabled", false);
        $(".seleccionado").children("input").prop("disabled", true);
        console.log("modificar");
        modificando = true;
        comprobarAccion("m");
        setH1("Modificando fila");
    });

    comprobarAccion("a");
    $("button").click(function () {
        if ($(".seleccionado").hasClass("primeraFila")) {
            comprobarAccion("p");
        }
        if ($(".seleccionado").hasClass("ultimaFila")) {
            comprobarAccion("f");
        }
        if (!modificando) {
            comprobarAccion("b");
        } else {
            comprobarAccion("m");
        }


    });

    function setH1(text) {
        $("h1").text(text);
    }

    $('body').keyup(function (e) {

        if (e.keyCode == 38) {
            $(".menosuno").click();
        }
        if (e.keyCode == 39) {
            $(".fin").click();

        }
        if (e.keyCode == 40) {
            $(".masuno").click();

        }
        if (e.keyCode == 37) {
            $(".inicio").click();

        }
    });
});



